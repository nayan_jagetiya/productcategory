package com.brevitaz.productCategory.reexercise.validate;

import com.brevitaz.productCategory.reexercise.exception.InValidDataException;
import com.brevitaz.productCategory.reexercise.exception.NoContentException;
import com.brevitaz.productCategory.reexercise.model.Category;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.provider.EmptySource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Validate;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CategoryValidateTest {
    private static CategoryValidate categoryValidate;
    @BeforeAll
    public static void setUpOnce(){
        categoryValidate=new CategoryValidate();
    }

    @DisplayName("Validate Null Category")
    @Order(0)
    @Test
    public void testValidateNull(){
        assertThrows(InValidDataException.class,()->categoryValidate.validate(null));
    }

    @DisplayName("Validate Incorrect Entries: No Data")
    @Order(1)
    @Test
    public void testValidateInvalidDataNoData(){
        Category category=new Category();
        assertThrows(InValidDataException.class,()->categoryValidate.validate(category));
    }

    @DisplayName("Validate Incorrect Entries: Empty Category Name")
    @Order(1)
    @Test
    public void testValidateInvalidDataEmptyCategoryName(){
        Category category=new Category();
        category.setCategoryName("");
        assertThrows(InValidDataException.class,()->categoryValidate.validate(category));
    }

    @DisplayName("Validate Valid Category")
    @Order(2)
    @Test
    public void testValidateValidData(){
        Category category=new Category(2,"Mobile");
        assertTrue(categoryValidate.validate(category));
    }

    @DisplayName("Validate Invalid Categories")
    @Test
    public void testValidateInValidCategories(){
        assertThrows(InValidDataException.class,()->categoryValidate.validateCategories(null));
        List<Category> categories=new ArrayList<>();
        assertThrows(InValidDataException.class,()->categoryValidate.validateCategories(categories));

    }

    @AfterAll
    public static void clearUpOnce(){
        categoryValidate=null;
    }
}