package com.brevitaz.productCategory.reexercise.validate;

import com.brevitaz.productCategory.reexercise.exception.InValidDataException;
import com.brevitaz.productCategory.reexercise.exception.NoContentException;
import com.brevitaz.productCategory.reexercise.model.Category;
import com.brevitaz.productCategory.reexercise.model.Product;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.provider.NullSource;
import org.mockito.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ProductValidateTest {

    @InjectMocks
    private ProductValidate productValidate;
    @Mock
    private CategoryValidate categoryValidate;
//
    public ProductValidateTest(){
        MockitoAnnotations.openMocks(this);
    }


    @Order(0)
    @DisplayName("Validate Null Product")
    @Test
    public void testValidateNullProduct(){
        assertThrows(NoContentException.class,()->productValidate.validate(null));
    }

    @Order(1)
    @DisplayName("Validate Invalid Entries: No Product Data")
    @Test
    public void testValidateNoProductData(){
        Product product=new Product();
        assertThrows(InValidDataException.class,()->productValidate.validate(product));
    }

    @Order(2)
    @DisplayName("Validate Invalid Entries: Empty Product Name")
    @Test
    public void testValidateEmptyProductName(){
        Product product=new Product();
        product.setProductName("");
        assertThrows(InValidDataException.class,()->productValidate.validate(product));
    }

    @Order(3)
    @DisplayName("Validate Invalid Entries: InValid Date")
    @Test
    public void testValidateInValidDate(){
        Product product=new Product();
        product.setProductName("Huawei Mate Pro RS");
        assertThrows(InValidDataException.class,()->productValidate.validate(product));
        try {
            product.setLaunchDate(new SimpleDateFormat("dd/MM/yyyy").parse("21/06/1018"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assertThrows(InValidDataException.class,()->productValidate.validate(product));
    }

    @Order(4)
    @DisplayName("Validate Correct Product")
    @Test
    public void testValidateValidProduct(){
        Product p=new Product();
        p.setProductName("Redmi Note 8 Pro");
        try {
            p.setLaunchDate(new SimpleDateFormat("dd/MM/yyyy").parse("27/02/2040"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Category> categories=new ArrayList<>();
        categories.add(new Category(1,"Mobiles"));
        p.setCategories(categories);
        assertTrue(productValidate.validate(p));
    }

}