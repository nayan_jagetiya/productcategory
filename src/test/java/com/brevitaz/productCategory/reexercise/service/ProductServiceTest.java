package com.brevitaz.productCategory.reexercise.service;

import com.brevitaz.productCategory.reexercise.dao.CategoryDao;
import com.brevitaz.productCategory.reexercise.dao.ProductCategoryDao;
import com.brevitaz.productCategory.reexercise.dao.ProductDao;
import com.brevitaz.productCategory.reexercise.exception.*;
import com.brevitaz.productCategory.reexercise.model.Category;
import com.brevitaz.productCategory.reexercise.model.Pagination;
import com.brevitaz.productCategory.reexercise.model.Product;
import com.brevitaz.productCategory.reexercise.model.ProductSort;
import com.brevitaz.productCategory.reexercise.validate.ProductValidate;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ProductServiceTest {

    @InjectMocks
    private static ProductServiceImpl productService;
    @Mock
    private static ProductDao productDaoMock;
    @Mock
    private static CategoryDao categoryDaoMock;
    @Mock
    private static ProductCategoryDao productCategoryDaoMock;

    @Mock
    public static ProductValidate productValidateMock;

    public ProductServiceTest(){
        MockitoAnnotations.openMocks(this);
    }

    @Nested
    @DisplayName("Add Product")
    class AddProduct {
        private Product productToAdd;
        private int id;

        @BeforeEach
        public void setUp() {
            id = 1;
            productToAdd = new Product();

            productToAdd.setProductId(id);
            productToAdd.setProductName("ABC");
            productToAdd.setLaunchDate(new Date(System.currentTimeMillis() + 30000000));
            productToAdd.setPrice(123456);
            productToAdd.setCategories(new ArrayList() {{
                add(new Category(12, "XYZ"));
            }});
            BDDMockito.given(productValidateMock.validate(Mockito.any(Product.class))).willReturn(true);
        }
        @AfterEach
        public void clear(){
            productToAdd=null;
            id=0;
        }

        @DisplayName("Product Already Exists")
        @Test
        void testProductAlreadyExists() throws SQLException {
            BDDMockito.given(productDaoMock.productExists(productToAdd)).willReturn(true);
            assertEquals(productToAdd,productService.addProduct(productToAdd));
        }

        @DisplayName("UniqueProduct: Product Insertion Unsuccessful in Main DB")
        @Test
        void testProductInsertionUnsuccessful() throws SQLException {
            BDDMockito.given(productDaoMock.productExists(productToAdd)).willReturn(false);
            BDDMockito.given(productDaoMock.insert(productToAdd)).willReturn(-1);
            assertThrows(DataNotAddedException.class,()->productService.addProduct(productToAdd));
        }

        @DisplayName("UniqueProduct: Category Insertion Unsuccessful")
        @Test
        void testCategoryInsertionUnsuccessful() throws SQLException {
            BDDMockito.given(productDaoMock.productExists(productToAdd)).willReturn(false);
            BDDMockito.given(productDaoMock.insert(productToAdd)).willReturn(productToAdd.getProductId());
            BDDMockito.given(categoryDaoMock.categoryExists(productToAdd.getCategories().get(0))).willReturn(false);
            BDDMockito.given(categoryDaoMock.insert(productToAdd.getCategories().get(0))).willReturn(-1);
            assertThrows(DataNotAddedException.class,()->productService.addProduct(productToAdd));
        }


        /*
        * Category Inserted -> -> Not Available in Dependant Db -> Not Inserted in DependantDb
        * Category AlreadyExist -> Not Available in Dependant DB -> Not Inserted in DependantDb
        *
        * Category Inserted -> -> Not Available in Dependant Db -> InsertionSuccessFul , Returned Product
        * Category AlreadyExist -> Not Available in Dependant DB -> InsertionSuccessful , Returned Product
        *
        * Category Inserted -> -> Available in Dependant Db -> Returned Product
        * Category AlreadyExist -> Available in Dependant DB -> Returned Product
        * */
        @DisplayName("Not Inserted in Dependent DB")
        @Nested
        class ProductNotInsertedInDependantDb {
            @Test
            @DisplayName("CategoryInserted But DependantDb Not Updated")
            void testNotInsertedInDependantDbWhenCategoryInsertionSuccessful() throws SQLException {
                BDDMockito.given(productDaoMock.productExists(productToAdd)).willReturn(false);
                BDDMockito.given(productDaoMock.insert(productToAdd)).willReturn(productToAdd.getProductId());

                Category category = productToAdd.getCategories().get(0);

                BDDMockito.given(categoryDaoMock.categoryExists(category)).willReturn(false);
                BDDMockito.given(categoryDaoMock.insert(category)).willReturn(category.getCategoryId());
                BDDMockito.given(productCategoryDaoMock.productCategoryExists(productToAdd.getProductId(),category.getCategoryId())).willReturn(false);
                BDDMockito.given(productCategoryDaoMock.insert(productToAdd.getProductId(), category.getCategoryId())).willReturn(-1);
                assertThrows(DataNotAddedException.class, () -> productService.addProduct(productToAdd));
            }

            @Test
            @DisplayName("Category Already Exists But DependantDb Not Updated")
            void testNotInsertedInDependantDbWhenCategoryAlreadyExists() throws SQLException {
                BDDMockito.given(productDaoMock.productExists(productToAdd)).willReturn(false);
                BDDMockito.given(productDaoMock.insert(productToAdd)).willReturn(productToAdd.getProductId());

                Category category = productToAdd.getCategories().get(0);

                BDDMockito.given(categoryDaoMock.categoryExists(category)).willReturn(true);
                BDDMockito.given(productCategoryDaoMock.productCategoryExists(productToAdd.getProductId(),category.getCategoryId())).willReturn(false);
                BDDMockito.given(productCategoryDaoMock.insert(productToAdd.getProductId(), category.getCategoryId())).willReturn(-1);
                assertThrows(DataNotAddedException.class, () -> productService.addProduct(productToAdd));
            }
        }

        @DisplayName("Product Successfully Added")
        @Nested
        class ProductSuccessfullyAdded{
            @Test
            @DisplayName("Category Added and Dependent DB Updated")
            void testInsertedInDependantDbWhenCategoryInserted() throws SQLException {
                BDDMockito.given(productDaoMock.productExists(productToAdd)).willReturn(false);
                BDDMockito.given(productDaoMock.insert(productToAdd)).willReturn(productToAdd.getProductId());

                Category category = productToAdd.getCategories().get(0);

                BDDMockito.given(categoryDaoMock.categoryExists(category)).willReturn(false);
                BDDMockito.given(categoryDaoMock.insert(category)).willReturn(category.getCategoryId());
                BDDMockito.given(productCategoryDaoMock.productCategoryExists(productToAdd.getProductId(),category.getCategoryId())).willReturn(false);
                BDDMockito.given(productCategoryDaoMock.insert(productToAdd.getProductId(), category.getCategoryId())).willReturn(Mockito.anyInt());
                assertEquals(productToAdd, productService.addProduct(productToAdd));
            }

            @Test
            @DisplayName("Category Already Exists and Dependent DB Updated")
            void testInsertedInDependantDbWhenCategoryAlreadyExists() throws SQLException {
                BDDMockito.given(productDaoMock.productExists(productToAdd)).willReturn(false);
                BDDMockito.given(productDaoMock.insert(productToAdd)).willReturn(productToAdd.getProductId());

                Category category = productToAdd.getCategories().get(0);

                BDDMockito.given(categoryDaoMock.categoryExists(category)).willReturn(true);
                BDDMockito.given(productCategoryDaoMock.productCategoryExists(productToAdd.getProductId(),category.getCategoryId())).willReturn(false);
                BDDMockito.given(productCategoryDaoMock.insert(productToAdd.getProductId(), category.getCategoryId())).willReturn(Mockito.anyInt());
                assertEquals(productToAdd, productService.addProduct(productToAdd));
            }

            @Test
            @DisplayName("Category Added and ProductInsertion Successful")
            void testProductInsertedWhenCategoryInserted() throws SQLException {
                BDDMockito.given(productDaoMock.productExists(productToAdd)).willReturn(false);
                BDDMockito.given(productDaoMock.insert(productToAdd)).willReturn(productToAdd.getProductId());

                Category category = productToAdd.getCategories().get(0);

                BDDMockito.given(categoryDaoMock.categoryExists(category)).willReturn(false);
                BDDMockito.given(categoryDaoMock.insert(category)).willReturn(category.getCategoryId());
                BDDMockito.given(productCategoryDaoMock.productCategoryExists(productToAdd.getProductId(),category.getCategoryId())).willReturn(true);

                BDDMockito.given(productDaoMock.getProductById(productToAdd.getProductId())).willReturn(productToAdd);
                BDDMockito.given(categoryDaoMock.getCategories(productToAdd.getProductId())).willReturn(productToAdd.getCategories());

                assertEquals(productToAdd, productService.addProduct(productToAdd));
            }

            @Test
            @DisplayName("Category Already Exists and ProductInsertion Successful")
            void testProductInsertedWhenCategoryAlreadyExists() throws SQLException {
                BDDMockito.given(productDaoMock.productExists(productToAdd)).willReturn(false);
                BDDMockito.given(productDaoMock.insert(productToAdd)).willReturn(productToAdd.getProductId());

                Category category = productToAdd.getCategories().get(0);
                BDDMockito.given(categoryDaoMock.categoryExists(category)).willReturn(true);
                BDDMockito.given(productCategoryDaoMock.productCategoryExists(productToAdd.getProductId(),category.getCategoryId())).willReturn(true);

                BDDMockito.given(productDaoMock.getProductById(productToAdd.getProductId())).willReturn(productToAdd);
                BDDMockito.given(categoryDaoMock.getCategories(productToAdd.getProductId())).willReturn(productToAdd.getCategories());

                assertEquals(productToAdd, productService.addProduct(productToAdd));
            }
        }
    }

    @Nested
    @DisplayName("Get Product by Id")
    class GetProductById {

        private Product product;
        private int prodId;
        @BeforeEach
        public void setUp(){
            prodId=1;
            product=new Product();
            product.setProductId(prodId);
            product.setProductName("ABC");
            product.setLaunchDate(new Date(System.currentTimeMillis()+30000000));
            product.setPrice(123456);
            product.setCategories(new ArrayList(){{add(new Category(12,"XYZ"));}});
        }
        @AfterEach
        public void clear(){
            product=null;
            prodId=0;
        }

        @DisplayName("Test Negative Id")
        @ParameterizedTest
        @ValueSource(ints = {-1,-100,-10})
        void testNegativeId(int id) {
            assertThrows(ConstraintViolationException.class,()->productService.getProductById(id));
        }

        @DisplayName("Test for Null Product")
        @Test
        void testForNullProduct() throws SQLException {
            BDDMockito.given(productDaoMock.getProductById(Mockito.anyInt())).willReturn(null);
            assertThrows(ProductNotFoundException.class,()->productService.getProductById(Mockito.anyInt()));
        }

        @DisplayName("Test for Null Category")
        @Test
        void testForNullCategory() throws SQLException {
            BDDMockito.given(productDaoMock.getProductById(Mockito.anyInt())).willReturn(product);
            product.setCategories(null);
            BDDMockito.given(categoryDaoMock.getCategories(prodId)).willReturn(product.getCategories());
            assertThrows(CategoryNotFoundException.class,()->productService.getProductById(prodId));
        }


        @DisplayName("Test for Empty Category")
        @Test
        void testForEmptyCategory() throws SQLException {
            BDDMockito.given(productDaoMock.getProductById(Mockito.anyInt())).willReturn(product);
            product.setCategories(new ArrayList<>());
            BDDMockito.given(categoryDaoMock.getCategories(prodId)).willReturn(product.getCategories());
            assertThrows(CategoryNotFoundException.class,()->productService.getProductById(prodId));
        }


        @DisplayName("Product Retrieved Successfully")
        @Test
        void testForProductRetrieved() throws SQLException {
            BDDMockito.given(productDaoMock.getProductById(Mockito.anyInt())).willReturn(product);
            BDDMockito.given(categoryDaoMock.getCategories(prodId)).willReturn(product.getCategories());
            assertEquals(product,productService.getProductById(prodId));
        }
    }

    @Nested
    @DisplayName("Get Products by Search")
    class GetProductsBySearch {

        private String search;
        private Pagination pagination;
        private ProductSort sort;
        private List<Product> products;
        @BeforeEach
        public void setUp(){
            search="";
            pagination=new Pagination();
            sort=new ProductSort();
            products=new ArrayList<>();

            Product p1=new Product("ABC",123456,new Date(System.currentTimeMillis()+100000000),
                    new ArrayList(){{add(new Category(12,"XYZ"));}});
            p1.setProductId(1);

            Product p2=new Product("IPhone",123456,new Date(System.currentTimeMillis()+200000000),
                    new ArrayList(){{add(new Category(13,"Mobiles"));}});
            p1.setProductId(2);
            products.add(p1);
            products.add(p2);
        }

        @AfterEach
        public void clear(){
            search=null;pagination=null;sort=null;
            products=null;
        }

        @Test
        @DisplayName("Test Null Products")
        void testNullProducts() throws SQLException {
            BDDMockito.given(productDaoMock.getProducts(search,pagination,sort)).willReturn(null);
            assertThrows(ProductNotFoundException.class,()->productService.getProducts(search,pagination,sort));
        }

        @DisplayName("Test Empty Products")
        @Test
        void testEmptyProducts() throws SQLException {
            BDDMockito.given(productDaoMock.getProducts(search,pagination,sort)).willReturn(new ArrayList<>());
            assertThrows(ProductNotFoundException.class,()->productService.getProducts(search,pagination,sort));
        }

        @Test
        @DisplayName("Get valid products")
        public void testGetValidProducts() throws SQLException {
            Product product= products.get(0);
            Product product1= products.get(1);
            BDDMockito.given(productDaoMock.getProducts(search,pagination,sort)).willReturn(products);
            BDDMockito.given(categoryDaoMock.getCategories(product.getProductId())).willReturn(product.getCategories());
            BDDMockito.given(categoryDaoMock.getCategories(product1.getProductId())).willReturn(product1.getCategories());

            assertEquals(products,productService.getProducts(search,pagination,sort));

        }


    }

    @Nested
    @DisplayName("Get Products By Category Ids")
    class GetProductsByCategoryIds {
        @Disabled("Still to make use in controller")
        @Test
        void getProductsByCategoryIds() {

        }
    }

    @Nested
    @DisplayName("Update Product")
    class UpdateProduct {

        private Product productToUpdate;
        private int id;
        @BeforeEach
        public void setUp(){
            id=1;
            productToUpdate=new Product();

            productToUpdate.setProductId(id);
            productToUpdate.setProductName("ABC");
            productToUpdate.setLaunchDate(new Date(System.currentTimeMillis()+30000000));
            productToUpdate.setPrice(123456);
            productToUpdate.setCategories(new ArrayList(){{add(new Category(12,"XYZ"));}});
            BDDMockito.given(productValidateMock.validate(Mockito.any(Product.class))).willReturn(true);
        }
        @AfterEach
        public void clear(){
            productToUpdate=null;
            id=0;
        }
        @DisplayName("Product With Given Id is Null")
        @Test
        void testProductWithGivenIdIsNull() throws SQLException {
            BDDMockito.given(productDaoMock.getProductById(productToUpdate.getProductId())).willReturn(null);
            assertThrows(ProductNotFoundException.class,()->productService.updateProduct(productToUpdate));
        }

        @DisplayName("No Product Update Available")
        @Test
        void testNoProductUpdateAvailable() throws SQLException {
            BDDMockito.given(productDaoMock.getProductById(productToUpdate.getProductId())).willReturn(productToUpdate);
            assertEquals(productToUpdate,productService.updateProduct(productToUpdate));
        }

        @DisplayName("Product Found, but Categories Unavailable with given Product Id")
        @Test
        void testNoCategoryFound() throws SQLException {
            Product originalProduct=new Product();
            originalProduct.setProductId(productToUpdate.getProductId());
            originalProduct.setProductName("One Plus Nord");
            originalProduct.setLaunchDate(new Date(System.currentTimeMillis()+30000000));
            originalProduct.setPrice(41000.0);

            BDDMockito.given(productDaoMock.getProductById(productToUpdate.getProductId())).willReturn(originalProduct);
            productToUpdate.setCategories(null);
            BDDMockito.given(productDaoMock.update(productToUpdate)).willReturn(productToUpdate);
            BDDMockito.given(categoryDaoMock.getCategories(productToUpdate.getProductId())).willReturn(originalProduct.getCategories());

            assertThrows(CategoryNotFoundException.class,()->productService.updateProduct(productToUpdate));
        }

        @DisplayName("Update Completed with valid Product")
        @Test
        void testProductUpdated() throws SQLException {
            Product originalProduct=new Product();
            originalProduct.setProductId(1);
            originalProduct.setProductName("One Plus Nord");
            originalProduct.setLaunchDate(new Date(System.currentTimeMillis()+30000000));
            originalProduct.setPrice(41000.0);
            originalProduct.setCategories(productToUpdate.getCategories());

            BDDMockito.given(productDaoMock.getProductById(1)).willReturn(originalProduct);
            BDDMockito.given(productDaoMock.update(productToUpdate)).willReturn(productToUpdate);
            BDDMockito.given(categoryDaoMock.getCategories(1)).willReturn(originalProduct.getCategories());

            assertEquals(productToUpdate,productService.updateProduct(productToUpdate));
        }
    }

    @Nested
    @DisplayName("Delete Product")
    class DeleteProduct {
        @DisplayName("Product Deleted")
        @Test
        public void testValidDeleted() throws SQLException {
            int id=2;
            BDDMockito.given(productCategoryDaoMock.deleteProduct(id)).willReturn(true);
            BDDMockito.given(productDaoMock.delete(id)).willReturn(true);
            assertTrue(productService.deleteProduct(id));
        }

        @DisplayName("Product Not Deleted from dependant DB")
        @ParameterizedTest
        @ValueSource(booleans = {true,false})
        public void testNotDeletedFromDependentDB(boolean mainDbResult) throws SQLException {
            int id=1;
            BDDMockito.given(productCategoryDaoMock.deleteProduct(id)).willReturn(false);
            BDDMockito.given(productDaoMock.delete(id)).willReturn(mainDbResult);
            assertThrows(DataNotDeletedException.class,()->productService.deleteProduct(id));
        }

        @DisplayName("Product Not Deleted from Main DB")
        @ParameterizedTest
        @ValueSource(booleans = {true,false})
        public void testNotDeletedFromMainDB(boolean dependantDbResult) throws SQLException {
            int id=1;
            BDDMockito.given(productCategoryDaoMock.deleteProduct(id)).willReturn(dependantDbResult);
            BDDMockito.given(productDaoMock.delete(id)).willReturn(false);
            assertThrows(DataNotDeletedException.class,()->productService.deleteProduct(id));
        }

    }
}