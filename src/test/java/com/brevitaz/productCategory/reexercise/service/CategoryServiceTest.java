package com.brevitaz.productCategory.reexercise.service;

import com.brevitaz.productCategory.reexercise.dao.CategoryDao;
import com.brevitaz.productCategory.reexercise.exception.CategoryNotFoundException;
import com.brevitaz.productCategory.reexercise.exception.ConstraintViolationException;
import com.brevitaz.productCategory.reexercise.model.Category;
import com.brevitaz.productCategory.reexercise.model.CategorySort;
import com.brevitaz.productCategory.reexercise.model.Pagination;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CategoryServiceTest {

    @InjectMocks
    private CategoryServiceImpl categoryService;
    @Mock
    private CategoryDao categoryDaoMock;

    public CategoryServiceTest(){
        MockitoAnnotations.openMocks(this);
    }

    @Nested
    @DisplayName("Get Categories By Id")
    class GetCategoriesById {

        @DisplayName("Check for Invalid Id: Negative Id")
        @ParameterizedTest
        @ValueSource(ints = {-1,-10,-100})
        void testNegativeId(int id) {
            assertThrows(ConstraintViolationException.class,()->categoryService.getCategoriesById(id));
        }

        @DisplayName("Check for Empty Categories")
        @Test
        void testEmptyCategories() throws SQLException {
            BDDMockito.given(categoryDaoMock.getCategories(2)).willReturn(new ArrayList<>());
            assertThrows(CategoryNotFoundException.class,()->categoryService.getCategoriesById(2));
        }

        @DisplayName("Check for Valid Categories")
        @Test
        void testValidCategories() throws SQLException {
            List<Category> categories = new ArrayList() {{
                add(new Category(10, "Electronics"));
                add(new Category(11, "Mobile"));
            }};
            BDDMockito.given(categoryDaoMock.getCategories(1)).willReturn(categories);

            assertEquals(categories,categoryService.getCategoriesById(1));
        }

    }

    @Nested
    @DisplayName("Get Categories By Search")
    class GetCategoriesBySearch {
        private String search;
        private Pagination pagination;
        private CategorySort sort;
        @BeforeEach
        public void setUp(){
            search="";
            pagination=new Pagination();
            sort=new CategorySort();
        }
        @AfterEach
        public void clear(){
            search=null;pagination=null;sort=null;
        }

        @DisplayName("Check for Empty Categories")
        @Test
        void testEmptyCategories() throws SQLException {
            BDDMockito.given(categoryDaoMock.getCategories("Fruits",pagination,sort)).willReturn(new ArrayList<>());
            assertThrows(CategoryNotFoundException.class,()->categoryService.getCategories("Fruits",pagination,sort));
        }

        @DisplayName("Check for Valid Categories")
        @Test
        void testValidCategories() throws SQLException {
            List<Category> categories = new ArrayList() {{
                add(new Category(10, "Electronics"));
                add(new Category(11, "Mobile"));
            }};
            BDDMockito.given(categoryDaoMock.getCategories(search,pagination,sort)).willReturn(categories);
            assertEquals(categories,categoryService.getCategories(search,pagination,sort));
        }
    }
}