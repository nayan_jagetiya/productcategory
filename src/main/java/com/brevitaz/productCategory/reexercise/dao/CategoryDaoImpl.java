package com.brevitaz.productCategory.reexercise.dao;

import com.brevitaz.productCategory.reexercise.config.MyLoggerConfig;
import com.brevitaz.productCategory.reexercise.model.Category;
import com.brevitaz.productCategory.reexercise.model.CategorySort;
import com.brevitaz.productCategory.reexercise.model.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
public class CategoryDaoImpl implements CategoryDao {
    @Autowired
    private Connection connection;

    private static final String RETRIEVE_CATEGORIES="SELECT cat_id,cat_name FROM CATEGORY WHERE cat_id IN (SELECT cat_id FROM PRODUCTCATEGORY WHERE prod_id=?)";

    private static final String INSERT="INSERT INTO CATEGORY VALUES(cat_id.nextval,?)";
    private static final String RETRIEVE="SELECT cat_id,cat_name FROM (SELECT ROWNUM RNUM,C.* FROM CATEGORY C) WHERE RNUM>? AND RNUM<=?";
    private static final String RETRIEVE_ID="SELECT cat_id FROM CATEGORY WHERE cat_name=?";

    private Logger logger;
    public CategoryDaoImpl(){
        this.logger=new MyLoggerConfig().setLogger(Logger.getLogger(CategoryDaoImpl.class.getName())).getMyLogger();
    }
    @Override
    public int insert(Category category) throws SQLException {
        logger.log(Level.INFO,"Started inserting Category from dao..");
        PreparedStatement ps=connection.prepareStatement(INSERT);
        ps.setString(1,category.getCategoryName());
        ps.executeUpdate();
        int id=getId(category);
        logger.log(Level.INFO,"Category Added from dao with id : {0}",id);
        return id;
    }

    @Override
    public List<Category> getCategories(int productId) throws SQLException {
        logger.log(Level.INFO,"started getting categories from dao with prod id {0}",productId);
        PreparedStatement ps=connection.prepareStatement(RETRIEVE_CATEGORIES);
        ps.setInt(1,productId);
        ResultSet rs=ps.executeQuery();

        List<Category> categories=new ArrayList<>();
        while (rs.next()){
            Category category=new Category();
            category.setCategoryId(rs.getInt(1));
            category.setCategoryName(rs.getString(2));
            logger.log(Level.FINER,"got category {0}",category);
            categories.add(category);
        }
        logger.log(Level.INFO,"returned categories from dao having prod Id {0}",productId);
        return categories;
    }

    @Override
    public List<Category> getCategories(String search, Pagination page, CategorySort sort) throws SQLException {
        logger.log(Level.INFO,"Starting getting message from service");
        PreparedStatement ps=connection.prepareStatement(RETRIEVE);
        ps.setInt(1,page.getOffset());
        ps.setInt(2,page.getLimit());
        ResultSet rs=ps.executeQuery();
        List<Category> categories=new ArrayList<>();
        while(rs.next()){
            categories.add(new Category(rs.getInt(1),rs.getString(2)));
        }
        logger.log(Level.INFO,"returned categories from dao..");
        logger.log(Level.FINER,"returned categories from dao are: {0}",categories);
        return categories;
    }

    private int getId(Category category) throws SQLException {
        PreparedStatement ps=connection.prepareStatement(RETRIEVE_ID);
        ps.setString(1,category.getCategoryName());
        ResultSet rs=ps.executeQuery();
        if (rs.next())
            return rs.getInt(1);
        return -1;
    }

    @Override
    public boolean categoryExists(Category category) throws SQLException {
        logger.log(Level.INFO,"Started checking whether Category Exists...");
        PreparedStatement ps=connection.prepareStatement(RETRIEVE_ID);
        ps.setString(1,category.getCategoryName());
        if(!(ps.executeQuery().next())){
            logger.log(Level.INFO,"Category Not Found from dao");
            return false;
        }
        logger.log(Level.INFO,"Category Already Exists");
        return true;
    }

    @Override
    public void closeConnection() throws SQLException {
        connection.close();
    }
}
