package com.brevitaz.productCategory.reexercise.dao;

import com.brevitaz.productCategory.reexercise.config.MyLoggerConfig;
import com.brevitaz.productCategory.reexercise.exception.ProductCategoryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
public class ProductCategoryDaoImpl implements ProductCategoryDao {
    @Autowired
    private Connection connection;

    private static final String INSERT="INSERT INTO PRODUCTCATEGORY VALUES(pc_id.nextval,?,?)";
    private static final String RETRIEVE_ID ="SELECT pc_id FROM PRODUCTCATEGORY WHERE prod_id=? AND cat_id=?";
    private static final String DELETE_USING_CATEGORY="DELETE FROM PRODUCTCATEGORY WHERE cat_id=?";
    private static final String DELETE_USING_PRODUCT="DELETE FROM PRODUCTCATEGORY WHERE prod_id=?";

    private Logger logger;
    public ProductCategoryDaoImpl(){
        this.logger=new MyLoggerConfig().setLogger(Logger.getLogger(ProductCategoryDaoImpl.class.getName())).getMyLogger();
    }
    @Override
    public int insert(int productId, int categoryId) throws SQLException {
        logger.log(Level.INFO,"Starting inserting data in Product Category..");
        PreparedStatement ps= connection.prepareStatement(INSERT);
        ps.setInt(1,productId);
        ps.setInt(2,categoryId);
        ps.executeUpdate();
        int id=getId(productId,categoryId);

        logger.log(Level.INFO,"ProductCategory added from dao having id {0}",id);
        return id;
    }

    @Override
    public boolean deleteProduct(int productId) throws SQLException {
        logger.log(Level.INFO,"started to delete from product category where product id is {0}",productId);
        PreparedStatement ps= connection.prepareStatement(DELETE_USING_PRODUCT);
        ps.setInt(1,productId);
        if(!(ps.executeUpdate()>0)){
            logger.log(Level.INFO,"Deletion from product category unsuccessful");
            return false;
        }
        logger.log(Level.INFO,"Deletion from product category successful");
        return true;
    }

    @Override
    public boolean deleteCategory(int categoryId) throws SQLException {
        PreparedStatement ps= connection.prepareStatement(DELETE_USING_CATEGORY);
        ps.setInt(1,categoryId);
        return ps.executeUpdate()>0;
    }

     private int getId(int productId, int categoryId) throws SQLException {
        PreparedStatement ps= connection.prepareStatement(RETRIEVE_ID);
        ps.setInt(1,productId);
        ps.setInt(2,categoryId);
        ResultSet rs= ps.executeQuery();
        if (rs.next())
            rs.getInt(1);
        return -1;
    }
    public boolean productCategoryExists(int productId,int categoryId) throws SQLException {
        logger.log(Level.INFO,"Started checking whether ProductCategory Exists...");
        PreparedStatement ps= connection.prepareStatement(RETRIEVE_ID);
        ps.setInt(1,productId);
        ps.setInt(2,categoryId);
        if(!(ps.executeQuery().next())){
            logger.log(Level.SEVERE,"Product and Category Not Found from dao");
            return false;
        }
        logger.log(Level.INFO,"ProductCategory Already Exists");
        return true;
    }

    @Override
    public void closeConnection() throws SQLException {
        connection.close();
    }
}
