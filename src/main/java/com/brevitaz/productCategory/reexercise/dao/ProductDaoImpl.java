package com.brevitaz.productCategory.reexercise.dao;

import com.brevitaz.productCategory.reexercise.config.MyLoggerConfig;
import com.brevitaz.productCategory.reexercise.model.Pagination;
import com.brevitaz.productCategory.reexercise.model.Product;
import com.brevitaz.productCategory.reexercise.model.ProductSort;
import com.brevitaz.productCategory.reexercise.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
public class ProductDaoImpl implements ProductDao {

    @Autowired
    private Connection connection;
    private Logger logger;
    public ProductDaoImpl(){
        this.logger=new MyLoggerConfig().setLogger(Logger.getLogger(ProductDaoImpl.class.getName())).getMyLogger();
    }

    private static final String INSERT="INSERT INTO PRODUCT VALUES(prod_id.nextval,?,?,?)";
    private static final String UPDATE="UPDATE PRODUCT SET prod_name=?, price=?, launch_date=? WHERE prod_id=?";
    private static final String DELETE="DELETE FROM PRODUCT WHERE prod_id=?";
    private static final String RETRIEVE_PROD_ID ="SELECT prod_id from PRODUCT WHERE prod_name=? and price=? and launch_date=?";
    private static final String RETRIEVE_PRODUCT_BY_ID="SELECT prod_name,price,launch_date FROM PRODUCT WHERE prod_id=?";
    private static final String RETRIEVE_PRODUCTS="SELECT prod_id,prod_name,price,launch_date from(SELECT ROWNUM RNUM,P.* FROM PRODUCT P WHERE lower(P.prod_name) LIKE lower(?) ORDER BY %s %s) WHERE RNUM>? AND RNUM<=?";
    private static final String RETRIEVE_PRODUCTS1="SELECT prod_id,prod_name,price,launch_date FROM (SELECT ROWNUM RNUM,P.* from PRODUCT p WHERE prod_id IN (?) ORDER BY %s %s) WHERE RNUM>? AND RNUM<=?";

    @Override
    public int insert(Product product) throws SQLException{
        logger.log(Level.INFO,"Started inserting products from dao...");
        PreparedStatement ps= connection.prepareStatement(INSERT);
        ps.setString(1, product.getProductName());
        ps.setDouble(3, product.getPrice());
        ps.setDate(2,new java.sql.Date(product.getLaunchDate().getTime()));
        ps.executeUpdate();

        int id=getId(product);
        if (id==-1){;
            logger.log(Level.SEVERE,"Data Insertion Unsuccessful from dao");
            return id;
        }
        logger.log(Level.INFO,"Product inserted with new id: {0}",id);
        return id;
    }

    @Override
    public Product getProductById(int productId) throws SQLException {
        logger.log(Level.INFO,"Started getting product from dao...");
        PreparedStatement ps= connection.prepareStatement(RETRIEVE_PRODUCT_BY_ID);
        ps.setInt(1,productId);
        ResultSet rs=ps.executeQuery();
        Product product=null;
        if (!rs.next()) {
            logger.log(Level.SEVERE,"Product Not Found from dao");
            logger.log(Level.FINER,"Product Not Found from dao");
            return null;
        }
        product = new Product(rs.getString("prod_name"), rs.getDouble("price"), rs.getDate("launch_date"));
        product.setProductId(productId);
        logger.log(Level.INFO,"returned product having id {0} in dao",productId);
        logger.log(Level.FINER,"returned product having id {0}: {1} in dao",new Object[]{productId,product});
        return product;
    }

    @Override
    public List<Product> getProducts(String searchString, Pagination pagination, ProductSort sort) throws SQLException {
        logger.log(Level.INFO,"started getting products from dao...");
        logger.log(Level.FINER,"searchString is :{0}, pagination : {1} , sorting: {2}",new Object[]{searchString,pagination,sort});

        List<Product> products=new ArrayList<>();
        String myQuery=String.format(RETRIEVE_PRODUCTS,sort.getSortCriteria().toString(),sort.getSortOrder().toString());

        PreparedStatement ps= connection.prepareStatement(myQuery);
        ps.setString(1,searchString+"%");
        ps.setInt(2,pagination.getOffset());
        ps.setInt(3,pagination.getOffset()+pagination.getLimit());
        ResultSet rs=ps.executeQuery();
        while (rs.next()){
            Product product=new Product();
            product.setProductId(rs.getInt(1));
            product.setProductName(rs.getString(2));
            product.setPrice(rs.getDouble(3));
            product.setLaunchDate(rs.getDate(4));
            logger.log(Level.FINER,"product is {0}",product);
            products.add(product);
        }

        logger.log(Level.FINER,"products in dao are:{0}",products);
        logger.log(Level.INFO,"returned products from dao ");
        return products;
    }

    @Override
    public List<Product> getProducts(List<Integer> categoryIds, Pagination page, ProductSort sort) throws SQLException {
        logger.log(Level.INFO,"Started getting products from dao having category ids {0}",categoryIds);
        String myQuery=String.format(RETRIEVE_PRODUCTS1,sort.getSortCriteria(),sort.getSortOrder());
        PreparedStatement ps= connection.prepareStatement(myQuery);
        Array array= connection.createArrayOf("INTEGER",categoryIds.toArray());
        ps.setArray(1,array);
        ps.setInt(2,page.getOffset());
        ps.setInt(3,page.getLimit());
        ResultSet rs=ps.executeQuery();

        List<Product> products=new ArrayList<>();
        while(rs.next()){
            Product product=null;
            product.setProductId(rs.getInt(1));
            product.setProductName(rs.getString(2));
            product.setPrice(rs.getDouble(3));
            product.setLaunchDate(rs.getDate(4));
            products.add(product);
        }
        logger.log(Level.FINER,"returned products from dao having category ids {0} are : {1}", new Object[]{categoryIds, products});
        logger.log(Level.INFO,"returned product ids from dao having category ids {0} ",categoryIds);
        return products;
    }

    @Override
    public Product update(Product product) throws SQLException {
        logger.log(Level.INFO,"started updating Product from dao having id {0}...",product.getProductId());
        PreparedStatement ps= connection.prepareStatement(UPDATE);
        ps.setString(1,product.getProductName());
        ps.setDouble(2,product.getPrice());
        ps.setDate(3,new java.sql.Date(product.getLaunchDate().getTime()));
        ps.setInt(4,product.getProductId());
        ps.executeUpdate();
        product=getProductById(product.getProductId());
        logger.log(Level.INFO,"updated product from dao with id {0}",product.getProductId());
        logger.log(Level.FINER,"new updated product from dao is: ",product);
        return product;
    }

    @Override
    public boolean delete(int productId) throws SQLException {
        logger.log(Level.INFO,"starting delete from dao having id :{0}...", productId);
        PreparedStatement ps= connection.prepareStatement(DELETE);
        ps.setInt(1,productId);
        if(!(ps.executeUpdate()>0)){
            logger.log(Level.SEVERE,"Delete Unsuccessful from dao");
            return false;
        }
        logger.log(Level.INFO,"product deleted having product Id: {0}",productId);
        return true;
    }

    public int getId(Product product) throws SQLException {
        PreparedStatement ps = connection.prepareStatement(RETRIEVE_PROD_ID);
        ps.setString(1, product.getProductName());
        ps.setDouble(2,product.getPrice());
        ps.setDate(3,new java.sql.Date(product.getLaunchDate().getTime()));
        ResultSet rs=ps.executeQuery();
        while (rs.next())
            return rs.getInt(1);
        return -1;
    }

    @Override
    public boolean productExists(Product product) throws SQLException {
        logger.log(Level.INFO,"Started checking whether Product Exists...");
        PreparedStatement ps= connection.prepareStatement(RETRIEVE_PROD_ID);
        ps.setString(1,product.getProductName());
        ps.setDouble(2,product.getPrice());
        ps.setDate(3,new java.sql.Date(product.getLaunchDate().getTime()));
        if(!(ps.executeQuery().next())){
            logger.log(Level.SEVERE,"Product Not Found from dao");
        }
        logger.log(Level.INFO,"Product Already Exists");
        return true;
    }

    @Override
    public void closeConnection() throws SQLException {
        connection.close();
    }

}
