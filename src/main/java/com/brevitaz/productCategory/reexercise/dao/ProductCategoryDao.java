package com.brevitaz.productCategory.reexercise.dao;

import java.sql.SQLException;

public interface ProductCategoryDao {
    int insert(int productId, int categoryId) throws SQLException;

    boolean deleteProduct(int productId) throws SQLException;

    boolean deleteCategory(int categoryId) throws SQLException;

    boolean productCategoryExists(int productId,int categoryId) throws SQLException;

    void closeConnection() throws SQLException;
}
