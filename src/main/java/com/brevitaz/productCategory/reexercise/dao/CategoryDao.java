package com.brevitaz.productCategory.reexercise.dao;

import com.brevitaz.productCategory.reexercise.model.Category;
import com.brevitaz.productCategory.reexercise.model.CategorySort;
import com.brevitaz.productCategory.reexercise.model.Pagination;

import java.sql.SQLException;
import java.util.List;

public interface CategoryDao {
    int insert(Category category) throws SQLException;

    List<Category> getCategories(int productId) throws SQLException;

    List<Category> getCategories(String search, Pagination page, CategorySort sort) throws SQLException;

    boolean categoryExists(Category category) throws SQLException;

    void closeConnection() throws SQLException;
}
