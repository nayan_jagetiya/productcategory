package com.brevitaz.productCategory.reexercise.dao;

import com.brevitaz.productCategory.reexercise.model.Pagination;
import com.brevitaz.productCategory.reexercise.model.Product;
import com.brevitaz.productCategory.reexercise.model.ProductSort;

import java.sql.SQLException;
import java.util.List;

public interface ProductDao {
    int insert(Product product) throws SQLException;

    Product getProductById(int productId) throws SQLException;

    List<Product> getProducts(String searchString, Pagination pagination, ProductSort sort) throws SQLException;

    List<Product> getProducts(List<Integer> categoryIds, Pagination page, ProductSort sort) throws SQLException;

    Product update(Product product) throws SQLException;

    boolean delete(int productId) throws SQLException;

    int getId(Product product) throws SQLException;

    boolean productExists(Product product) throws SQLException;

    void closeConnection() throws SQLException;
}
