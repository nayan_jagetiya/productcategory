package com.brevitaz.productCategory.reexercise.service;

import com.brevitaz.productCategory.reexercise.config.MyLoggerConfig;
import com.brevitaz.productCategory.reexercise.dao.CategoryDao;
import com.brevitaz.productCategory.reexercise.dao.ProductDaoImpl;
import com.brevitaz.productCategory.reexercise.exception.CategoryNotFoundException;
import com.brevitaz.productCategory.reexercise.exception.ConstraintViolationException;
import com.brevitaz.productCategory.reexercise.model.Category;
import com.brevitaz.productCategory.reexercise.model.CategorySort;
import com.brevitaz.productCategory.reexercise.model.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class CategoryServiceImpl implements CategoryService{

    @Autowired
    private CategoryDao categoryDao;

    private Logger logger;
    public CategoryServiceImpl(){
        this.logger=new MyLoggerConfig().setLogger(Logger.getLogger(CategoryServiceImpl.class.getName())).getMyLogger();
    }
//    getCategoriesByProductId
    public List<Category> getCategoriesById(int productId) throws SQLException {
        logger.log(Level.INFO,"Started getting categories from service...");
        if(productId<0) {
            RuntimeException e= new ConstraintViolationException("Enter valid id");
            logger.log(Level.SEVERE,e.getMessage()+" from service");
            logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
            throw e;
        }
        List<Category> categories=categoryDao.getCategories(productId);
        if(categories==null ||  categories.isEmpty()) {
            RuntimeException e=new CategoryNotFoundException("categories not found");
            logger.log(Level.SEVERE,e.getMessage()+" from service");
            logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
            throw e;
        }
        logger.log(Level.INFO,"returned categories from service");
        logger.log(Level.FINER,"returned categories from service are: {0}",categories);
        return categories;
    }

//    getAllCategories
    public List<Category> getCategories(String search,Pagination page, CategorySort sort) throws SQLException {
        logger.log(Level.INFO,"Starting getting products from service...");

        logger.log(Level.FINER,"Pagination: {0}",page);
        page.setOffset(page.getPageCount() * page.getLimit());
        logger.log(Level.FINER,"Pagination: {0}",page);

        List<Category> categories= categoryDao.getCategories(search,page,sort);
        if(categories.isEmpty()) {
            RuntimeException e=new CategoryNotFoundException("categories not found");
            logger.log(Level.SEVERE,e.getMessage()+" from service");
            logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
            throw e;
        }
        logger.log(Level.INFO,"returned categories from service");
        logger.log(Level.FINER,"returned categories from service are: {0}",categories);
        return categories;
    }

    public void closeConnection() throws SQLException {
        categoryDao.closeConnection();
    }

}