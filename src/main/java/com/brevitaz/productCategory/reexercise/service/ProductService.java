package com.brevitaz.productCategory.reexercise.service;

import com.brevitaz.productCategory.reexercise.model.Pagination;
import com.brevitaz.productCategory.reexercise.model.Product;
import com.brevitaz.productCategory.reexercise.model.ProductSort;

import java.sql.SQLException;
import java.util.List;

public interface ProductService {
    List<Product> getProducts(String searchString, Pagination page, ProductSort sort) throws SQLException;
    List<Product> getProductsByCategoryIds(List<Integer> categoryIds, Pagination page, ProductSort sort) throws SQLException;
    Product getProductById(int id) throws SQLException;
    Product addProduct(Product product) throws SQLException;
    Product updateProduct(Product product) throws SQLException;
    boolean deleteProduct(int id) throws SQLException;
    void closeConnection() throws SQLException;

}
