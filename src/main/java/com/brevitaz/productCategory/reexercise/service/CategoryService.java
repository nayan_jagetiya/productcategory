package com.brevitaz.productCategory.reexercise.service;

import com.brevitaz.productCategory.reexercise.model.Category;
import com.brevitaz.productCategory.reexercise.model.CategorySort;
import com.brevitaz.productCategory.reexercise.model.Pagination;

import java.sql.SQLException;
import java.util.List;

public interface CategoryService {
    List<Category> getCategoriesById(int productId) throws SQLException;
    List<Category> getCategories(String search,Pagination page, CategorySort sort) throws SQLException ;
    void closeConnection() throws SQLException;
}
