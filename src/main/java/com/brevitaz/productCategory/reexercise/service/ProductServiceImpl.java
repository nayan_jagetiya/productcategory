package com.brevitaz.productCategory.reexercise.service;

import com.brevitaz.productCategory.reexercise.config.MyLoggerConfig;
import com.brevitaz.productCategory.reexercise.dao.CategoryDao;
import com.brevitaz.productCategory.reexercise.dao.ProductCategoryDao;
import com.brevitaz.productCategory.reexercise.dao.ProductDao;
import com.brevitaz.productCategory.reexercise.exception.*;
import com.brevitaz.productCategory.reexercise.model.Category;
import com.brevitaz.productCategory.reexercise.model.Pagination;
import com.brevitaz.productCategory.reexercise.model.Product;
import com.brevitaz.productCategory.reexercise.model.ProductSort;
import com.brevitaz.productCategory.reexercise.validate.ProductValidate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService{
    @Autowired
    private ProductDao productDao;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private ProductCategoryDao productCategoryDao;
    @Autowired
    ProductValidate productValidate;

    private Logger logger;

    public ProductServiceImpl(){
        this.logger=new MyLoggerConfig().setLogger(Logger.getLogger(ProductServiceImpl.class.getName())).getMyLogger();
    }


    public Product addProduct(Product product) throws SQLException {
        logger.log(Level.INFO,"Started adding product from service...");
        int productId, categoryId;

//        if validation successful, remove white spaces from category name and product name
        if(productValidate.validate(product)) {
            product.setProductName(product.getProductName().trim());
            for(Category c:product.getCategories()){
                c.setCategoryName(c.getCategoryName().trim());
            }
        }


        if (productDao.productExists(product)) {
            product.setProductId(productDao.getId(product));
            logger.log(Level.INFO, "product already existed and returned from service having id:{0}",product.getProductId());
            logger.log(Level.FINER, "product already existed and returned from service: ",product);
            return product;
        }
        else {
            if((productId=productDao.insert(product))==-1) {
                RuntimeException e=new DataNotAddedException("Product not added, Unsuccessful Operation,try again...");
                logger.log(Level.SEVERE,e.getMessage()+" from service");
                logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
                throw e;
            }
            else{
                product.setProductId(productId);
            }
        }
        for (Category category : product.getCategories()) {
            if (!categoryDao.categoryExists(category)) {
                if((categoryId=categoryDao.insert(category))==-1){
                    RuntimeException e=new DataNotAddedException("Category not added...");
                    logger.log(Level.SEVERE,e.getMessage()+" from service");
                    logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
                    throw e;
                }
                else {
                    category.setCategoryId(categoryId);
                }
            }

            if (productCategoryDao.productCategoryExists(product.getProductId(), category.getCategoryId())) {
                logger.log(Level.INFO,"ProductCategory Already Exists from service.. so returned existing product having id: {0}",product.getProductId());
                return getProductById(product.getProductId());
            }
            else if(productCategoryDao.insert(product.getProductId(), category.getCategoryId())==-1) {
                RuntimeException e=new DataNotAddedException("Product Category Not Added...");
                logger.log(Level.SEVERE,e.getMessage()+" from service");
                logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
                throw e;
            }
        }
        logger.log(Level.INFO,"Product Added Successfully from service and generated id is: {0}",product.getProductId());
        logger.log(Level.FINER,"Product Added Successfully from service having details: {0}",product);
        return product;
    }

//    getProductById
    public Product getProductById(int id) throws SQLException {
        logger.log(Level.INFO,"started getting product from service....");
        if (id<0) {
            RuntimeException e=new ConstraintViolationException("Enter valid id");
            logger.log(Level.SEVERE,e.getMessage());
            logger.log(Level.FINER,e.getMessage(),e.getCause());
            throw e;
        }
        Product product=productDao.getProductById(id);
        if(product==null) {
            RuntimeException e = new ProductNotFoundException("Product not found");
            logger.log(Level.SEVERE,e.getMessage()+" from service");
            logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
            throw e;
        }
        List<Category> categories=categoryDao.getCategories(id);
        if(categories==null || categories.isEmpty()){
            RuntimeException e=new CategoryNotFoundException("Category not found");
            logger.log(Level.SEVERE,e.getMessage()+" in service class");
            logger.log(Level.FINER,e.getMessage(),e);
            throw e;
        }
        product.setCategories(categories);
        logger.log(Level.INFO,"returned product from service with id {0} having category ids {1}",new Object[]{id, categories.stream().map(Category::getCategoryId).collect(Collectors.toList())});
        logger.log(Level.FINER,"returned product with id {0} :{1}",new Object[]{id, product});
        return product;
    }

//    getProductsBySearchString
    public List<Product> getProducts(String searchString,Pagination page, ProductSort sort) throws SQLException {
        logger.log(Level.INFO,"started getting products from service...");
        logger.log(Level.FINER,"searchString is :{0}, pagination : {1} , sorting: {2}",new Object[]{searchString,page,sort});
        page.setOffset(page.getPageCount() * page.getLimit());   // for pageCount starting from zero
        logger.log(Level.FINER,"pagination : {0}",page);

        List<Product> products=productDao.getProducts(searchString,page,sort);
        if(products==null || products.isEmpty()) {
            RuntimeException e=new ProductNotFoundException("No Product Found");
            logger.log(Level.SEVERE,e.getMessage()+" in service layer before assigning categories");
            throw e;
        }
        for (Product product:products){
            List<Category> categories = categoryDao.getCategories(product.getProductId());
            logger.log(Level.FINER,"got categories having product id {0} in service : ", new Object[] {product.getProductId(),categories});
            product.setCategories(categories);
        }

        logger.log(Level.FINER,"returned products from service: {0}",products);
        logger.log(Level.INFO,"returned products from service having ids : {0}",products.stream().map(Product::getProductId).collect(Collectors.toList()));
        return products;
    }

//    getProductsByCategoryIds
    public List<Product> getProductsByCategoryIds(List<Integer> categoryIds,Pagination page, ProductSort sort) throws SQLException {
        logger.log(Level.INFO,"Started getting products from service having category ids {0}",categoryIds);
        if(categoryIds == null || categoryIds.isEmpty()){
            RuntimeException e=new InValidDataException("Enter proper Category ids");
            logger.log(Level.SEVERE,e.getMessage()+" in service");
            logger.log(Level.FINER,e.getMessage()+" in service",e.getCause());
            throw e;
        }

        logger.log(Level.FINER,"Pagination:{0}",page);
        page.setOffset(page.getPageCount() * page.getLimit());
        logger.log(Level.FINER,"Pagination:{0}",page);

        List<Product> products=productDao.getProducts(categoryIds,page,sort);
        if(products.isEmpty()) {
            RuntimeException e= new ProductNotFoundException("No Product Found");
            logger.log(Level.SEVERE,e.getMessage()+" in service");
            logger.log(Level.FINER,e.getMessage()+" in service",e.getCause());
            throw e;
        }
        logger.log(Level.INFO,"returned product ids from service having category ids {0} : {1}",new Object[]{categoryIds,products.stream().map(Product::getProductId).collect(Collectors.toList())});
        logger.log(Level.INFO,"returned products from service having category ids {0} : {1} ",new Object[]{categoryIds,products});
        return products;
    }

    public Product updateProduct(Product product) throws SQLException {
        logger.log(Level.INFO,"started updating product from service having id {0}...",product.getProductId());
        productValidate.validate(product);
        Product originalProduct=productDao.getProductById(product.getProductId());
        if(originalProduct==null){
            RuntimeException e=new ProductNotFoundException("Product Not Found");
            logger.log(Level.SEVERE,e.getMessage()+" from service");
            logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
            throw e;
        }
        else if(!originalProduct.equals(product)){
            product=productDao.update(product);
            List<Category> categories=categoryDao.getCategories(product.getProductId());
            if(categories==null || categories.isEmpty()){
                RuntimeException e=new CategoryNotFoundException("Category not found with given product Id}");
                logger.log(Level.SEVERE,e.getMessage()+" in service class");
                logger.log(Level.FINER,e.getMessage(),e);
                throw e;
            }
            product.setCategories(categories);
        }
        logger.log(Level.INFO,"updated and returned product from service having id:{0}",product.getProductId());
        logger.log(Level.FINER,"updated and returned product from service:{0}",product);
        return product;
    }

    public boolean deleteProduct(int prodId) throws SQLException {
        logger.log(Level.INFO,"starting delete from service having id :{0}...", prodId);
        if(!productCategoryDao.deleteProduct(prodId))
        {
            RuntimeException e= new DataNotDeletedException("Product not deleted from dependent DB Table");
            logger.log(Level.SEVERE,e.getMessage()+" from service");
            logger.log(Level.FINE,e.getMessage()+" from service",e.getCause());
            throw e;
        }
        if(!productDao.delete(prodId)) {
            RuntimeException e= new DataNotDeletedException("Product not deleted from Main DB Table");
            logger.log(Level.SEVERE,e.getMessage()+" from service");
            logger.log(Level.FINE,e.getMessage()+" from service",e.getCause());
            throw e;
        }
        logger.log(Level.INFO,"product with id {0} deleted from service",prodId);
        return true;
    }

    public void closeConnection() throws SQLException {
        productDao.closeConnection();
        categoryDao.closeConnection();
        productCategoryDao.closeConnection();
    }

}

