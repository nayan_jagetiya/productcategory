package com.brevitaz.productCategory.reexercise.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionController extends ResponseEntityExceptionHandler{

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<Object> handleProductNotFound(ProductNotFoundException ex) {
        return new ResponseEntity(ex.getMessage(),HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CategoryNotFoundException.class)
    public ResponseEntity<Object> handleCategoryNotFound(CategoryNotFoundException ex) {
        return new ResponseEntity(ex.getMessage(),HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(DataNotAddedException.class)
    public ResponseEntity<Object> handleDataNotAdded(DataNotAddedException ex){
        return new ResponseEntity(ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(DataNotDeletedException.class)
    public ResponseEntity<Object> handleDataNotDeleted(DataNotDeletedException ex){
        return new ResponseEntity(ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(InValidDataException.class)
    public ResponseEntity<Object> handleInValidData(InValidDataException ex){
        return new ResponseEntity(ex.getMessage(),HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex){
        return new ResponseEntity(ex.getMessage(),HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(NoContentException.class)
    public ResponseEntity<Object> handleNoContent(NoContentException ex){
        return new ResponseEntity(ex.getMessage(),HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception ex){
        return new ResponseEntity(ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
