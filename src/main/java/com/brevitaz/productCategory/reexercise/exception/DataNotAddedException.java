package com.brevitaz.productCategory.reexercise.exception;

//@ResponseStatus(HttpStatus.EXPECTATION_FAILED)
public class DataNotAddedException extends RuntimeException{
    public DataNotAddedException(String message){
        super(message);
    }
}
