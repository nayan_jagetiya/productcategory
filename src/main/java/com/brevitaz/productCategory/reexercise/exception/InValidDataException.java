package com.brevitaz.productCategory.reexercise.exception;

public class InValidDataException extends RuntimeException{
    public InValidDataException(String message){
        super(message);
    }
}
