package com.brevitaz.productCategory.reexercise.exception;

public class NoContentException extends RuntimeException {
    public NoContentException(String message){
        super(message);
    }
}
