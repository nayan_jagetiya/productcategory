package com.brevitaz.productCategory.reexercise.exception;

public class DataNotDeletedException extends RuntimeException {
    public DataNotDeletedException(String message) {
        super(message);
    }
}
