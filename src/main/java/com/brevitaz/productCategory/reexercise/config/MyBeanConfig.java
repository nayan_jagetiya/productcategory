package com.brevitaz.productCategory.reexercise.config;

import com.brevitaz.productCategory.reexercise.util.ConnectionProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.sql.Connection;
import java.sql.SQLException;

@Configuration
public class MyBeanConfig {
    @Profile({"Oracle","default"})
    @Bean("connection")
    Connection oracleConnection() throws ClassNotFoundException, SQLException {
        return new ConnectionProvider().getOracleConnection();
    }

    @Profile("MySql")
    @Bean("connection")
    Connection mySqlConnection() throws SQLException, ClassNotFoundException {
        return new ConnectionProvider().getMySqlConnection();
    }

    @Bean
    MyLoggerConfig myLoggerConfig(){
        return new MyLoggerConfig();
    }

}
