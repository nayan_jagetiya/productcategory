package com.brevitaz.productCategory.reexercise.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.logging.*;

public class MyLoggerConfig {
    private Handler handler;
    private Logger logger;
    private Formatter formatter;
    private static String dirName="logs1//";
    private static String fileName="MyLog.txt//";

    public MyLoggerConfig() {
        this.logger=LogManager.getLogManager().getLogger(Logger.GLOBAL_LOGGER_NAME);
        try {
            File f=new File(dirName);
            if(f.mkdirs())
                logger.log(Level.INFO,dirName+" created for logging...");
            this.handler = new FileHandler((dirName+fileName), 1024*1024,1,true);
        }
        catch (IOException e){
//            System.out.println(e);
            System.out.println(e.getMessage()+" : File "+dirName+fileName+" create some problem");

        }
        this.formatter=new SimpleFormatter();
    }

    public MyLoggerConfig setLogger(Logger logger){
        this.logger=logger;
        return this;
    }
    public MyLoggerConfig setHandler(Handler handler) {
        this.handler = handler;
        return this;
    }
    public MyLoggerConfig setFormatter(Formatter formatter){
        this.formatter=formatter;
        return this;
    }

    public Logger getMyLogger() {
        handler.setFormatter(formatter);
        logger.addHandler(handler);
        return logger;
    }

}
