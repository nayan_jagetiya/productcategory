package com.brevitaz.productCategory.reexercise.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

//@Service
public class ConnectionProvider {
    private Logger logger;
    public ConnectionProvider(){
        logger=Logger.getLogger(ConnectionProvider.class.getName());
    }
    public Connection getOracleConnection() throws ClassNotFoundException, SQLException {
        logger.log(Level.INFO,"Started getting connection: OracleDB");
        String url="jdbc:oracle:thin:@localhost:1521:XE";
        String usrname="Nayan";
        String psswd="root";
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        }
        catch (ClassNotFoundException ex){
            logger.log(Level.SEVERE,ex.getMessage());
            logger.log(Level.FINER,ex.getMessage(), ex.getCause());
        }
        logger.log(Level.INFO,"Oracle Driver loaded...");
        Connection connection=null;
        try {
             connection= DriverManager.getConnection(url, usrname, psswd);
        }
        catch (SQLException ex){
            logger.log(Level.SEVERE,ex.getMessage());
            logger.log(Level.FINER,ex.getMessage(), ex.getCause());
        }
        logger.log(Level.INFO,"Connection Established...");
        return connection;
    }

    public Connection getMySqlConnection(){
        logger.log(Level.INFO,"Started getting connection: MySqlDB");
        String url="jdbc:mysql://localhost/brevitazjdbcproject";
        String usrname="root";
        String psswd="root";

        try {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (ClassNotFoundException ex){
            logger.log(Level.SEVERE,ex.getMessage());
            logger.log(Level.FINER,ex.getMessage(), ex.getCause());
        }
        logger.log(Level.INFO,"MySqlConnector Driver loaded...");
        Connection connection=null;
        try {
            connection= DriverManager.getConnection(url, usrname, psswd);
        }
        catch (SQLException ex){
            logger.log(Level.SEVERE,ex.getMessage());
            logger.log(Level.FINER,ex.getMessage(), ex.getCause());
        }
        logger.log(Level.INFO,"Connection Established...");
        return connection;
    }

}
