package com.brevitaz.productCategory.reexercise.model;

import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class PageResult<T>{
    private int pageNo;     // pageCount
    private long totalRecords; // offset
    private int pageSize;   // limit
    private List<T> data;   //

//    public PageResult(int pageNo, long totalRecords, int pageSize, List<T> data) {
//        this.pageNo = pageNo;
//        this.totalRecords = totalRecords;
//        this.pageSize = pageSize;
//        this.data = data;
//    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PageResult{" +
                "pageNo=" + pageNo +
                ", totalRecords=" + totalRecords +
                ", pageSize=" + pageSize +
                ", data=" + data +
                '}';
    }
}
