package com.brevitaz.productCategory.reexercise.model;

public class CategorySort {
    private Category.SortCriteria sortCriteria;
    private SortOrder sortOrder;
    public enum SortOrder{ASC,DESC}

    public CategorySort() {
        this.sortCriteria= Category.SortCriteria.CATEGORY_NAME;
        this.sortOrder=SortOrder.ASC;
    }

    public CategorySort(Category.SortCriteria sortCriteria, SortOrder sortOrder) {
        this.sortCriteria = sortCriteria;
        this.sortOrder = sortOrder;
    }

    public Category.SortCriteria getSortCriteria() {
        return sortCriteria;
    }

    public void setSortCriteria(Category.SortCriteria sortCriteria) {
        this.sortCriteria = sortCriteria;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }
}
