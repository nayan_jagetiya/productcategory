package com.brevitaz.productCategory.reexercise.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class Product {
    private int productId;
    private String productName;
    private double price;

    @JsonFormat(pattern="yyyy-MM-dd")
    public Date launchDate;
    private List<Category> categories;

    public enum SortCriteria{LAUNCH_DATE,PROD_NAME,PRICE,PROD_ID}

    public Product() {
        this.launchDate=new Date();
    }
    public Product(String productName,double price,Date date) {
        this.productName = productName;
        this.price=price;
        this.launchDate=date;
    }
    public Product(String productName,double price, Date launchDate, List<Category> categories) {
        this.productName = productName;
        this.launchDate = launchDate;
        this.categories = categories;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Date getLaunchDate() { return launchDate; }

    public void setLaunchDate(Date launchDate) {
        this.launchDate = launchDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productName='" + productName + '\'' +
                ", price=" + price +
                ", launchDate=" + launchDate +
                "\n, categories=" + categories +
                '}'+"\n\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (Double.compare(product.price, price) != 0) return false;
        if (!productName.equals(product.productName)) return false;
        return launchDate.equals(product.launchDate);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = productName.hashCode();
        result = 31 * result + launchDate.hashCode();
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
