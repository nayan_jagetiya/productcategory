package com.brevitaz.productCategory.reexercise.model;

import org.springframework.stereotype.Component;

@Component
public class Pagination {
//    private static final int DEFAULT_OFFSET=0;
//    private static final int DEFAULT_LIMIT=10;
    private int pageCount;
    private int limit;
    private int offset;

    public Pagination() {
        this.limit=10;
    }

    public Pagination(int pageCount, int limit) {
        this.pageCount = pageCount;
        this.limit = limit;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
