package com.brevitaz.productCategory.reexercise.model;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Category {
    private int categoryId;
    private String categoryName;
    private List<Product> products;

    public enum SortCriteria{CATEGORY_NAME,CATEGORY_ID}

    public Category() {}
    public Category(int categoryId,String categoryName) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                '}'+"\n";
    }
}
