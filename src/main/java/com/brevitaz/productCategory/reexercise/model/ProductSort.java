package com.brevitaz.productCategory.reexercise.model;

import org.springframework.stereotype.Component;

@Component
public class ProductSort {
    private Product.SortCriteria sortCriteria;
    private SortOrder sortOrder;

    public enum SortOrder{ASC,DESC}

    public ProductSort() {
        this.sortCriteria= Product.SortCriteria.LAUNCH_DATE;
        this.sortOrder= SortOrder.DESC;
    }

    public ProductSort(Product.SortCriteria sortCriteria, SortOrder sortOrder) {
        this.sortCriteria = sortCriteria;
        this.sortOrder = sortOrder;
    }

    public Product.SortCriteria getSortCriteria() {
        return sortCriteria;
    }

    public void setSortCriteria(Product.SortCriteria sortCriteria) {
        this.sortCriteria = sortCriteria;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }
}
