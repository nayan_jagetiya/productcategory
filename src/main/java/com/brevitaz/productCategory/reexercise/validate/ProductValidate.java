package com.brevitaz.productCategory.reexercise.validate;

import com.brevitaz.productCategory.reexercise.exception.InValidDataException;
import com.brevitaz.productCategory.reexercise.exception.NoContentException;
import com.brevitaz.productCategory.reexercise.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class ProductValidate {
    @Autowired
    private CategoryValidate categoryValidate;

    private Logger logger;
    public ProductValidate(){
        logger=Logger.getLogger(ProductValidate.class.getName());
    }
    public boolean validate(Product product){
        logger.log(Level.INFO,"Started Validating Product...");
        if(product==null) {
            RuntimeException e=new NoContentException("No Product Content");
            logger.log(Level.SEVERE,e.getMessage()+" from service");
            logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
            throw e;
        }
        else if(product.getProductName()==null || product.getProductName().trim().isEmpty()) {
            RuntimeException e = new InValidDataException("Enter valid Product Name");
            logger.log(Level.SEVERE,e.getMessage()+" from service");
            logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
            throw e;
        }
        else if( product.getLaunchDate()==null || product.getLaunchDate().before(new Date())){
            RuntimeException e = new InValidDataException("Enter valid Launch Date");
            logger.log(Level.SEVERE,e.getMessage()+" from service");
            logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
            throw e;
        }
        categoryValidate.validateCategories(product.getCategories());
        logger.log(Level.INFO,"Product Validation Successful.. ");
        return true;
    }
}
