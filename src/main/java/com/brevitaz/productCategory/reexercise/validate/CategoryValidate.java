package com.brevitaz.productCategory.reexercise.validate;

import com.brevitaz.productCategory.reexercise.exception.InValidDataException;
import com.brevitaz.productCategory.reexercise.exception.NoContentException;
import com.brevitaz.productCategory.reexercise.model.Category;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class CategoryValidate {
    private Logger logger;
    public CategoryValidate(){
        logger=Logger.getLogger(CategoryValidate.class.getName());
    }
    public boolean validate(Category category){
        logger.log(Level.INFO,"Started Validating Category...");
        if(category==null) {
            RuntimeException e=new InValidDataException("No Category Content");
            logger.log(Level.SEVERE,e.getMessage()+" from service");
            logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
            throw e;
        }
        else if(category.getCategoryName()==null || category.getCategoryName().trim().isEmpty()) {
            RuntimeException e=new InValidDataException("Enter valid category Name");
            logger.log(Level.SEVERE,e.getMessage()+" from service");
            logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
            throw e;
        }
        logger.log(Level.INFO,"Category Validation Successful... ");
        return true;
    }
    public boolean validateCategories(List<Category> categories){
        if(categories == null || categories.isEmpty()) {
            RuntimeException e = new InValidDataException("No Categories ...");
            logger.log(Level.SEVERE,e.getMessage()+" from service");
            logger.log(Level.FINER,e.getMessage()+" from service",e.getCause());
            throw e;
        }
        for(Category c:categories){
            validate(c);
        }
        return true;
    }
}
