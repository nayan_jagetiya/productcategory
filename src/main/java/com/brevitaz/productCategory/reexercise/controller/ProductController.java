package com.brevitaz.productCategory.reexercise.controller;

import com.brevitaz.productCategory.reexercise.config.MyLoggerConfig;
import com.brevitaz.productCategory.reexercise.exception.DataNotDeletedException;
import com.brevitaz.productCategory.reexercise.exception.ProductNotFoundException;
import com.brevitaz.productCategory.reexercise.model.Pagination;
import com.brevitaz.productCategory.reexercise.model.Product;
import com.brevitaz.productCategory.reexercise.model.ProductSort;
import com.brevitaz.productCategory.reexercise.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
public class ProductController {
    @Autowired
    ProductService productService;

    private Logger logger;
    public ProductController(){
        this.logger=new MyLoggerConfig().setLogger(Logger.getLogger(ProductController.class.getName())).getMyLogger();
    }

    @GetMapping("/product/all")
    public List<Product> getProducts() throws SQLException, IOException,SecurityException {
//            Logger logger = new MyLoggerConfig(Logger.getLogger(ProductController.class.getName())).getMyLogger();
            logger.log(Level.INFO, "Started getting products from controller....");
            List<Product> products = productService.getProducts("", new Pagination(), new ProductSort());
            if(products.isEmpty()){
                RuntimeException e =new ProductNotFoundException("Product not found");
                logger.log(Level.SEVERE,e.getMessage()+" in controller");
                logger.log(Level.FINER,e.getMessage()+" in controller",e.getCause());
                throw e;
            }
            logger.log(Level.FINER, "returned products from controller: {0}", products);
            logger.log(Level.INFO, "returned products from controller with id: {0}",products.stream().map(Product::getProductId).collect(Collectors.toList()));
            return products;
    }

    @GetMapping("/product/id={productId}")
    public Product getProduct(@PathVariable int productId) throws SQLException {
        logger.log(Level.INFO,"started getting product with id {0} from controller",productId);
        Product product=productService.getProductById(productId);
        if(product==null){
            RuntimeException e=new ProductNotFoundException("Product Not FOund");
            logger.log(Level.SEVERE,e.getMessage()+" in Controller");
            logger.log(Level.FINER,e.getMessage()+" in Controller",e.getCause());
            throw e;
        }
        logger.log(Level.FINER,"returned product :{0} from controller",product);
        logger.log(Level.INFO,"returned product from controller having id :{0}",product.getProductId());
        return product;
    }
    @PostMapping("/product")
    public Product addProduct(@RequestBody Product product) throws SQLException{
        logger.log(Level.INFO,"Started adding product in controller...");
        product=productService.addProduct(product);
        if(product==null){
            RuntimeException e=new ProductNotFoundException("Product Not Found");
            logger.log(Level.SEVERE,e.getMessage()+" from controller");
            logger.log(Level.FINER,e.getMessage()+" from controller",e.getCause());
            throw e;
        }
        logger.log(Level.INFO,"Product added and returned from controller having id : {0}",product.getProductId());
        logger.log(Level.INFO,"Product added and returned from controller having details: {0}",product);
        return product;
    }
    @PutMapping("/product")
    Product updateProduct(@RequestBody Product product) throws SQLException{
        logger.log(Level.INFO,"started updating product from controller having id {0}...",product.getProductId());
        logger.log(Level.FINER,"Product to update is: {0}",product);
        Product product1=productService.updateProduct(product);
        if(product.equals(product1)){
            logger.log(Level.INFO,"product already exists... no update required");
        }
        logger.log(Level.FINER,"returned Updated Product from controller:{0}...",product1);
        logger.log(Level.INFO,"returned Updated Product from controller having id {0}...",product1.getProductId());
        return product1;
    }
    @DeleteMapping("/product/id={productId)")
    boolean deleteProduct(@PathVariable int productId) throws SQLException{
        logger.log(Level.INFO,"starting delete from controller having id :{0}...", productId);
        boolean deleteStatus=productService.deleteProduct(productId);
        if (!deleteStatus){
            RuntimeException e=new DataNotDeletedException("Unable to delete data");
            logger.log(Level.SEVERE,e.getMessage()+" from controller");
            logger.log(Level.FINER,e.getMessage()+" from controller",e.getCause());
            throw e;
        }
        logger.log(Level.INFO,"deleted product from controller having id :{0}", productId);
        return deleteStatus;
    }

}
