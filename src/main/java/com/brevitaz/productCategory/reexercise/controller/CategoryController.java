package com.brevitaz.productCategory.reexercise.controller;

import com.brevitaz.productCategory.reexercise.config.MyLoggerConfig;
import com.brevitaz.productCategory.reexercise.dao.ProductDaoImpl;
import com.brevitaz.productCategory.reexercise.model.Category;
import com.brevitaz.productCategory.reexercise.model.CategorySort;
import com.brevitaz.productCategory.reexercise.model.Pagination;
import com.brevitaz.productCategory.reexercise.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    private Logger logger;
    public CategoryController(){
        this.logger=new MyLoggerConfig().setLogger(Logger.getLogger(CategoryController.class.getName())).getMyLogger();
    }
    @GetMapping("/category/id={productId}")
    List<Category> getCategories(@PathVariable int productId) throws SQLException {
        List<Category> categories = categoryService.getCategoriesById(productId);
        logger.log(Level.INFO,"Started getting categories having product id : {0}",productId);
        logger.log(Level.INFO,"returned categories having product id {0}",productId);
        logger.log(Level.FINER,"returned categories having product id {0} are : {1}", new Object[]{productId, categories});
        return categories;
    }

    @GetMapping("/category/all")
    List<Category> getCategories() throws SQLException{
        logger.log(Level.INFO,"Started getting categories...");
        List<Category> categories= categoryService.getCategories("",new Pagination(),new CategorySort());
        logger.log(Level.INFO,"returned categories");
        logger.log(Level.FINER,"returned categories : {0}", categories);
        return categories;
    }
}
